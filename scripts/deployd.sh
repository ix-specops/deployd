#!/bin/sh
################################################################################
#	▗▄▄            ▗▄▖            ▗▄▄                                          #
#	▐▛▀█           ▝▜▌            ▐▛▀█                                         #
#	▐▌ ▐▌ ▟█▙ ▐▙█▙  ▐▌   ▟█▙ ▝█ █▌▐▌ ▐▌                                        #
#	▐▌ ▐▌▐▙▄▟▌▐▛ ▜▌ ▐▌  ▐▛ ▜▌ █▖█ ▐▌ ▐▌                                        #
#	▐▌ ▐▌▐▛▀▀▘▐▌ ▐▌ ▐▌  ▐▌ ▐▌ ▐█▛ ▐▌ ▐▌                                        #
#	▐▙▄█ ▝█▄▄▌▐█▄█▘ ▐▙▄ ▝█▄█▘  █▌ ▐▙▄█                                         #
#	▝▀▀   ▝▀▀ ▐▌▀▘   ▀▀  ▝▀▘   █  ▝▀▀                                          #
#			  ▐▌              █▌                                               #
################################################################################
#     by matto & jnixon             # " I meant no harm, I most truly did not  #
#   yo, the hive haz u now.         #  But I had to grow bigger                #
# engage, colonize, and deploy!     #  So bigger I got "  - Doc S.             #
################################################################################
# man logger to do better logging
# use "tee" to execute and log what commands are run
################################################################################
# ZFS SEND REC TESTS
################################################################################
# zfs send -R \
#   tank/iocage/jails/UUID/root@worker1-bup \
#	| mbuffer -s 128k -m 1G 2>/dev/null | ssh -p 2200 $DEST \
#	"/usr/local/bin/mbuffer -s 128k -m 1G | zfs receive zroot/iocage"
################################################################################
# zfs send -R \
# 	tank/iocage/jails/2031b468-a543-11e5-b835-0cc47a329128/root@worker1-bup2 \
# 	| mbuffer -s 128k -m 1G 2>/dev/null | ssh -p 2200  \
# 	-i /home/deploy/.ssh/id_rsa deploy@queen.freehive.io \
#	"mbuffer -s 128k -m 1G | zfs receive zroot/iocage/worker1"
################################################################################
# zfs send -R \
#	tank/iocage/jails/2031b468-a543-11e5-b835-0cc47a329128/root@worker1-bup2 \
# 	| mbuffer -s 128k -m 1G 2>/dev/null | ssh stinger.deployd.io \
# 	"mbuffer -s 128k -m 1G | zfs receive zroot/home/backups"
################################################################################
#  sudo zfs allow -u deploy create,receive,rename,mount,share,send zroot
#  is how you grant a user perms to do it
#  also, pkg install mbuffer on sending & receiving system
#  using mbuffer makes sure to not overflow RAM
################################################################################

# If you want to debug the script, set debug = true
# When in debug mode,
# the script test variable expansion and echo the commands before they're run

DEBUG = True
# Check if we're debugging the script
debug() {
	if [ DEBUG = True ]
		then
			set -x
		else:wq
			echo "Hold on to your butts!"
}

# sudo make me a function to run commands as deploy user
do () {
	sudo -u deploy
}
### [deployd_00] Set Env variables #############################################
SCRIPT_DIR="/home/deploy"
SRC_HOST="staging-www.ixsystems.com:8084"
SRC_WWW="/usr/local/www/www-site4/"
DEST_HOST="staging-www.ixsystems.com:8085"
DEST_WWW="/usr/local/www/www-site5/"
### Use SSH Keys in phase two ##################################################
# USER=""
# SRC_SSH_PUBKEY=""
# DEST_SSH_PUBKEY=""
################################################################################

### [deployd_01] Pre-run checkpoint ############################################
# pre-event log
do tee >(logger -s "$(date "+%F-%H:%M") [deployd] BEGIN: pre-deployment checkpoint" 2>> ~/.deployd_logger)
# take a snapshot before we run
do ssh -p 2200 -i ~/.ssh/id_rsa -t drone.freehive.io \
	"sudo io-snapsql honeycomb-thick@deployd_$(date "+%F-%H:%M")-pre-check"
# post-event log
do tee >(logger -s "$(date "+%F-%H:%M") [deployd] END: pre-deployment checkpoint" 2>> ~/.deployd_logger)
### [deployd_02] Deactivate plugins ############################################
# pre-event log
do tee >(logger -s "$(date "+%F-%H:%M") [deployd] BEGIN: deactivating plugins" 2>> ~/.deployd_logger)
# wp ssh to deactivate plugins on $SRC machine
do wp ssh plugin deactivate lockdown-wp-admin revisr ixserver --host=$SRC
# wp ssh deactivate plugins on $DEST machine
do wp ssh plugin deactivate lockdown-wp-admin revisr ixserver --host=$DEST
# post-event log
do tee (logger -s "$(date "+%F-%H:%M") [deployd] END: deactivating plugins" 2>> ~/.deployd_logger)
### [deployd_03] $SRC database export ##########################################
# pre-event log
do tee >(logger -s "$(date "+%F-%H:%M") [deployd] BEGIN: exporting $SRC database" 2>> ~/.deployd_logger)
do wp ssh db export src_db.sql --host=$SRC
# post-event log
do tee >(logger -s "$(date "+%F-%H:%M") [deployd] END: exporting $SRC database" 2>> ~/.deployd_logger)
### [deployd_04] rsync $SRC to $DEST ###########################################
# pre-event log
do tee >(logger -s "$(date "+%F-%H:%M") [deployd] BEGIN: rsync from $SRC to $DEST" 2>> ~/.deployd_logger)
do rsync -avpP -e "ssh -i ~/.ssh/id_rsa" $SRC_WWW $DEST_WWW
# post-event log
do tee (logger -s "$(date "+%F-%H:%M") [deployd] END: rsync from $SRC to $DEST" 2>> ~/.deployd_logger)
### [deployd_05] import database to $DEST ######################################
# pre-event log
do echo "$(date "+%F-%H:%M") [deployd] BEGIN: import database to $DEST " \
														   >> ~/.deployd_history
do echo wp ssh db import src_db.sql --host=$DEST
# post-event log
do echo "$(date "+%F-%H:%M") [deployd] END: import database to $DEST " \
														   >> ~/.deployd_history
### [deployd_06] activate plugins ##############################################
# pre-event log
do echo "$(date "+%F-%H:%M") [deployd] BEGIN: activating plugins " \
														   >> ~/.deployd_history
do echo wp ssh plugin activate lockdown-wp-admin revisr ixserver --host=$SRC
# post-event log
do echo "$(date "+%F-%H:%M") [deployd] END: activating plugins " \
														   >> ~/.deployd_history
### [deployd_07] database search-replace  ######################################
# pre-event log
do echo "$(date "+%F-%H:%M") [deployd] BEGIN: running db search-replace  " \
														   >> ~/.deployd_history
do echo wp ssh search-replace $SRC $DEST --host=$DEST
# post-event log
do echo "$(date "+%F-%H:%M") [deployd] END: running db search-replace  " \
														   >> ~/.deployd_history
### [deployd_08] Post-run checkpoint ###########################################
# pre-event log
do echo "$(date "+%F-%H:%M") [deployd] BEGIN: pre-deployment checkpoint" \
														   >> ~/.deployd_history
# take a snapshot before we run
do echo ssh -p 2200 -i ~/.ssh/id_rsa -t drone.freehive.io \
	"sudo io-snapsql honeycomb-thick@deployd_$(date "+%F-%H:%M")-pre-check"
# post-event log
do echo "$(date "+%F-%H:%M") [deployd] END: post-deployment checkpoint" \
														   >> ~/.deployd_history
### [deployd_09] review event log ##############################################
do echo "Ding! Fries are done!!"
do less ~/.deployd_history
################################################################################
# 									Done									   #
################################################################################