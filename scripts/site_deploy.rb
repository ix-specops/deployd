#
#	DEPLOYD-io
#

require 'mina/bundler'
require 'mina/rails'
require 'mina/git'

# require 'mina/rbenv'
# require 'mina/rvm'

# Basic settings:
set :domain, 'facet.freehive.io'
set :deploy_to, '/home/matto/html/boilerplate_html'
set :repository, 'https://github.com/h5bp/html5-boilerplate'
set :branch, 'master'
set :environment, 'staging'
# For system-wide RVM install.
# set :rvm_path, '/usr/local/rvm/bin/rvm'

# They will be linked in the 'deploy:link_shared_paths' step.
set :shared_paths, ['config/database.sql', 'config/wp-config.php', 'log']

# Optional settings:
set :user, 'matto'
#   set :port, '2200'
#   set :forward_agent, true

task :environment do

# Put any custom mkdir's in here for when `mina setup` is ran.

  task :setup => :environment do
    queue! %[mkdir -p "#{deploy_to}/#{shared_path}/log"]
    queue! %[chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/log"]

    queue! %[mkdir -p "#{deploy_to}/#{shared_path}/config"]
    queue! %[chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/config"]

    queue! %[touch "#{deploy_to}/#{shared_path}/config/database.sql"]
    queue! %[touch "#{deploy_to}/#{shared_path}/config/wp-config.php"]
    queue  %[echo "-----> Be sure to edit '#{deploy_to}/#{shared_path}/config/database.sql' and 'wp-config.php'."]

  end
  #
  #  Backup DB, Rsync Uploads dir & DB
  #
  desc "Deploys the current version to the server."
  task :deploy => :environment do
    to :before_hook do
      #
      #  TODO: LOCAL: Before SSH
      #  Backup DB, Rsync Uploads dir & DB
      #
    end
    deploy do
      # Put things here that will makeup the content & assets of the site
      invoke :'git:clone'
      invoke :'deploy:link_shared_paths'
      invoke :'deploy:cleanup'

      to :launch do
        queue "mkdir -p #{deploy_to}/#{current_path}/tmp/"
        queue "touch #{deploy_to}/#{current_path}/tmp/restart.txt"
      end
    end
    #
    # TODO: task :post_hook
    #
  end
end
