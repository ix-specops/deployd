<?php 
/**
	Admin Page Framework v3.7.11 by Michael Uno 
	Generated by PHP Class Files Script Generator <https://github.com/michaeluno/PHP-Class-Files-Script-Generator>
	<http://en.michaeluno.jp/deployd>
	Copyright (c) 2013-2016, Michael Uno; Licensed under MIT <http://opensource.org/licenses/MIT> */
class Deployd_AdminPageFramework_Form_View___CSS_Field extends Deployd_AdminPageFramework_Form_View___CSS_Base {
    protected function _get() {
        return $this->_getFormFieldRules();
    }
    static private function _getFormFieldRules() {
        return "td.deployd-field-td-no-title {padding-left: 0;padding-right: 0;}.deployd-fields {display: table; width: 100%;table-layout: fixed;}.deployd-field input[type='number'] {text-align: right;} .deployd-fields .disabled,.deployd-fields .disabled input,.deployd-fields .disabled textarea,.deployd-fields .disabled select,.deployd-fields .disabled option {color: #BBB;}.deployd-fields hr {border: 0; height: 0;border-top: 1px solid #dfdfdf; }.deployd-fields .delimiter {display: inline;}.deployd-fields-description {margin-bottom: 0;}.deployd-field {float: left;clear: both;display: inline-block;margin: 1px 0;}.deployd-field label{display: inline-block; width: 100%;}.deployd-field .deployd-input-label-container {margin-bottom: 0.25em;}@media only screen and ( max-width: 780px ) { .deployd-field .deployd-input-label-container {margin-bottom: 0.5em;}} .deployd-field .deployd-input-label-string {padding-right: 1em; vertical-align: middle; display: inline-block; }.deployd-field .deployd-input-button-container {padding-right: 1em; }.deployd-field .deployd-input-container {display: inline-block;vertical-align: middle;}.deployd-field-image .deployd-input-label-container { vertical-align: middle;}.deployd-field .deployd-input-label-container {display: inline-block; vertical-align: middle; } .repeatable .deployd-field {clear: both;display: block;}.deployd-repeatable-field-buttons {float: right; margin: 0.1em 0 0.5em 0.3em;vertical-align: middle;}.deployd-repeatable-field-buttons .repeatable-field-button {margin: 0 0.1em;font-weight: normal;vertical-align: middle;text-align: center;}@media only screen and (max-width: 960px) {.deployd-repeatable-field-buttons {margin-top: 0;}}.deployd-sections.sortable-section > .deployd-section,.sortable .deployd-field {clear: both;float: left;display: inline-block;padding: 1em 1.32em 1em;margin: 1px 0 0 0;border-top-width: 1px;border-bottom-width: 1px;border-bottom-style: solid;-webkit-user-select: none;-moz-user-select: none;user-select: none; text-shadow: #fff 0 1px 0;-webkit-box-shadow: 0 1px 0 #fff;box-shadow: 0 1px 0 #fff;-webkit-box-shadow: inset 0 1px 0 #fff;box-shadow: inset 0 1px 0 #fff;-webkit-border-radius: 3px;border-radius: 3px;background: #f1f1f1;background-image: -webkit-gradient(linear, left bottom, left top, from(#ececec), to(#f9f9f9));background-image: -webkit-linear-gradient(bottom, #ececec, #f9f9f9);background-image: -moz-linear-gradient(bottom, #ececec, #f9f9f9);background-image: -o-linear-gradient(bottom, #ececec, #f9f9f9);background-image: linear-gradient(to top, #ececec, #f9f9f9);border: 1px solid #CCC;background: #F6F6F6;} .deployd-fields.sortable {margin-bottom: 1.2em; } .deployd-field .button.button-small {width: auto;} .font-lighter {font-weight: lighter;} .deployd-field .button.button-small.dashicons {font-size: 1.2em;padding-left: 0.2em;padding-right: 0.22em;}";
    }
    protected function _getVersionSpecific() {
        $_sCSSRules = '';
        if (version_compare($GLOBALS['wp_version'], '3.8', '<')) {
            $_sCSSRules.= ".deployd-field .remove_value.button.button-small {line-height: 1.5em; }";
        }
        if (version_compare($GLOBALS['wp_version'], '3.8', '>=')) {
            $_sCSSRules.= ".deployd-repeatable-field-buttons {margin: 2px 0 0 0.3em;} @media screen and ( max-width: 782px ) {.deployd-fieldset {overflow-x: hidden;}}";
        }
        return $_sCSSRules;
    }
}
