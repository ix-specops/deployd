<?php
/*
    Plugin Name:    Deployd
    Plugin URI:     https://www.ixsystems.com
    Description:    iX WP Deployment
    Author:         James Nixon
    Author URI:     https://www.freenas.org
    Version:        0.6.66
    Requirements:   PHP 5.2.4 or above, WordPress 3.4 or above. Admin Page Framework 3.0.0 or above
*/
include( dirname( __FILE__ ) . '/library/admin-page-framework.php' );
	class Deployd extends Deployd_AdminPageFramework {

	public function setUp() {
		$this->setRootMenuPage( 'Deployd' );
		$this->addSubMenuItems(
			array(
				'title'     => 'Options',
				'page_slug' => 'deployd_options'
			)
		);
		$this->addSubMenuItems(
			array(
				'title'     => 'Deploy',
				'page_slug' => 'deployd_deploy'
			)
		);
	}
		public function do_deployd_options() {
			?>

			<h3>Deployd Options</h3>
			<p>Provide the following:
				<br />
				<strong>Deployment Directory:</strong> an absolute path to where the deployment scripts are located (default is the plugin directory /path/to/wordpress/wp-content/plugins/deployd/)
				<br />
				<strong>User:</strong> the user account on both destination and source and will be used to execute shell scripts.
				<br />
				<strong>Source SSH Public Key:</strong> Paste the public ssh key for the source machine
				<br />
				<strong>Destination SSH Public Key:</strong> Paste the source ssh key for the source machine
				<br />
				<strong>Source:</strong> the wordpress instance to be deployed from, e.g., staging.site.com
				<br />
				<strong>Destination:</strong> the wordpress instance deployed to, e.g., production.site.com
			</p>

			<form method="post" action="/wp-content/plugins/deployd/env_settings.php">
				Deployment Script Directory: (absolute path to deployment scripts)  <br />
				<input type="text" name="deployd_env[0][script_dir]" size="35" />
				<br />
				User: (user to run the deployd script, must use existing shell account, do not use "www")  <br />
				<input type="text" name="deployd_env[0][user]" size="35" />
				<br />
				Source SSH Public Key: (paste id_rsa.pub here) <br />
				<input type="textbox" name="deployd_env[0][source_key]" size="35" />
				<br />
				Destination SSH Public Key: (paste id_rsa.pub here) <br />
				<input type="textbpx" name="deployd_env[0][dest_key]" size="35" />
				<br />
				Source: (deployed from... staging.url.com) <br />
				<input type="text" name="deployd_env[0][source]" size="35" />
				<br />
				Destination: (deployed to production.url.com) <br />
				<input type="text" name="deployd_env[0][dest]" size="35" />
				<br /> <br />

				<input type="submit" value="Save" />
			</form>

			<?php
		}
		public function do_deployd_deploy() {
			?>

			<h3>Deploy</h3>
			<p>Deploy or Migrate an existing Wordpress Instance to another host.</p>
			<button onSubmit="$SCRIPT_DIR/migrate.sh"></button>
			<?php
		}
new Deployd;